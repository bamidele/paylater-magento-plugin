<?php

class Mage_PayLater_Block_Adminhtml_Order_View_Tab_Query
    extends Mage_Adminhtml_Block_Template
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_chat = null;

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('paylater/order/view/tab/query.phtml');
    }

    public function getTabLabel() {
        return $this->__('Paylater Payment Information');
    }

    public function getTabTitle() {
        return $this->__('Paylater  Payment');
    }

    public function canShowTab() {
        return true;
    }

    public function isHidden() {
        return false;
    }

    public function getOrder(){
        return Mage::registry('current_order');
    }

    public function getSubmitUrl()
    {
        return $this->getUrl('paylater/standard/query?order_id='.$this->getOrder()->getRealOrderId().'&otv=');
    }
    public function getonclick(){
      $onclick = "submitAndReloadArea($('sales_order_view_tabs_adminhtml_order_view_tab_query_val').parentNode, '".$this->getSubmitUrl()."')";	
  	  return $onclick;
	}
	
	public function getOrderId()
	{
		return $this->getOrder()->getRealOrderId();
	}
	
	public function getPaylaterResponseDetails()
	{
				$order = $this->getOrder();
				$transactionId=$this->getOrder()->getRealOrderId();

				 $resource = Mage::getSingleton('core/resource');
				 $table = $resource->getTableName('mgn_paylater_api_debug');
				 $query = "SELECT order_id,transn_id, amount, vendor_id,vendor,note,transactioncode,transactionstatus,post_time FROM ".$table." WHERE order_id='".$transactionId."' ORDER BY id desc limit 0,1";
				 $read=  $resource->getConnection('core_read');
				 $query = $read->query($query); 
				 while($row=$query->fetch()) 
				 {
						$RESPONSE["order_id"] =$row['order_id'];
						$RESPONSE["transn_id"] =$row['transn_id'];
						$RESPONSE["amount"] =$row['amount'];
						$RESPONSE["vendor_id"] =$row['vendor_id'];
						$RESPONSE["vendor"] =$row['vendor'];
                                                $RESPONSE["transactioncode"] =$row['transactioncode'];
                                                $RESPONSE["transactionstatus"] =$row['transactionstatus'];
						$RESPONSE["post_time"] =$row['post_time'];
						
				}
				return $RESPONSE;
		 
	}
	
}