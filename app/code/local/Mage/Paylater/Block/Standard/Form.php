<?php
class Mage_Paylater_Block_Standard_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        $this->setTemplate('paylater/standard/form.phtml');
        parent::_construct();
    }

}