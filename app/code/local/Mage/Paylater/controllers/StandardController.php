<?php

class Mage_Paylater_StandardController extends Mage_Core_Controller_Front_Action
{
    public $isValidResponse = false;

    /**
     * Get singleton with Paylater strandard
     *

     */
    public function getStandard()
    {
        return Mage::getSingleton('paylater/standard');
    }

    /**
     * Get Config model
     *
     */
    public function getConfig()
    {
        return $this->getStandard()->getConfig();
    }

 /**
     *  Returns IPN URL
     *
     *  @return	  string IPB URL
     */
    public function getPaylaterIPNUrl ()
    {
		$url=$this->getConfig()->getVerifyUrl();
        return $url;
    }	
    /**
     *  Return debug flag
     *
     *  @return  boolean
     */
    public function getDebug ()
    {
        return $this->getStandard()->getDebug();
    }

    /**
     * When a customer chooses Paylater on Checkout/Payment page
     *
     */
    public function redirectAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setPaylaterStandardQuoteId($session->getQuoteId());

        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId($session->getLastRealOrderId());
        $order->addStatusToHistory(
            $order->getStatus(),
            Mage::helper('paylater')->__('Customer was redirected to Paylater')
        );
        $order->save();

        $this->getResponse()
            ->setBody($this->getLayout()
                ->createBlock('paylater/standard_redirect')
                ->setOrder($order)
                ->toHtml());

        $session->unsQuoteId();
    }

    public function getStatus($transref,$vendorid,$vendor){
            //Build the get variables
            $request = '?vendorid='.$vendorid.'&vendor='.$vendor.'&transref='.$transref;
            //the API url 
            //Change The API url to https://app.paylater.ng/gateway once you have been moved to the live environment
            $url = "https://sandbox.paylater.ng/gateway";
            // Get cURL resource
            $curl = curl_init();
            // Set some options
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url.$request,
                CURLOPT_USERAGENT => $vendor//Change this to your website name

            ));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            //Specify Response format
            //Format can be xml(application/xml), json(application/json),html(application/html),serialised PHP(application/php)
            'Accept: application/json'
            ));
            // Send the request & save response to $response
            $response = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);
            return $response;
     }
    /**
     *  Success response from Paylater
     *
     *  @return	  void
     */
    public function  successResponseAction()
    {
       $transactionid= $this->getRequest()->getParam('transref');
       $transactioncode= $this->getRequest()->getParam('code');
       $transactionstatus= $this->getRequest()->getParam('status');
        $this->preResponse();
		if (!$this->isValidResponse) {
            $this->_redirect('');
            return ;
        }
        
               $resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		$table = "mgn_paylater_api_debug";
		$query = "SELECT * FROM  {$table}  WHERE  transn_id='".$transactionid."'";
		$readresult=$writeConnection->query($query);	
		while($row=$readresult->fetch()) 
		{
			$order_id =$row['order_id'];
			$amount =$row['amount'];
                        $vendor_id=$row['vendor_id'];
                        $vendor=$row['vendor'];
			$ID_MOD =$row['id'];
			
		}
                
        $response= $this->getStatus($transactionid, $vendor_id, $vendor);
        $responseArray = json_decode($response,true);
        //update sql
                $sql="UPDATE {$table} SET transactioncode='".$transactioncode."',
                    transactionstatus='".$transactionstatus."',post_time='".time()."' WHERE id='".$ID_MOD."' ";
		$writeConnection->query($sql);
                  
		$order = Mage::getModel('sales/order');
                $order->loadByIncrementId($order_id);
		//$trans_no = $order->getPayment()->getAdditionalInformation('transaction_id');
		 
	    if ($transactioncode == 'P00') 
		{		
			 
				if (!$order->getId()) 
				{
						return false;
				}
				
				$order->addStatusToHistory(
					$order->getStatus(),
					Mage::helper('paylater')->__('Customer successfully returned from Paylater')
				);



			
         $order->getPayment()->setAdditionalInformation('transacton_reference', $vendor_id);
		   if ($this->saveInvoice($order))
			{
                    $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
             } else {
                    $newOrderStatus = $this->getConfig()->getNewOrderStatus() ?
                        $this->getConfig()->getNewOrderStatus() : Mage_Sales_Model_Order::STATE_NEW;
             }

			$order->addStatusToHistory(
			$order->getStatus(),
			Mage::helper('paylater')->__('Paylater Payment Complete. Order ID :'.$order_id.', Transaction ID :'.$transactionid.', Paylater Reference : '.$transactionid),1 );
			$order->save();
			$order->sendNewOrderEmail();		

			$session = Mage::getSingleton('checkout/session');
			$session->setQuoteId($session->getPaylaterStandardQuoteId(true));
			Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
			$success_msg  =  '<b>Order No : </b>'.$order_id;
			$success_msg .= '<br><b>Transcation ID : </b>'.$transactionid;
			$success_msg .= '<br><b>Paylater Transcation Reference : </b>'.$transactionid;
			$success_msg .= '<BR><b>Reason : </b><span style="color:#0000ff;">'.$transactionstatus.'</span>';
			
			//Email To Customer
			$billing = $order->getBillingAddress();	
			$cust_name = $billing->getFirstname().' '.$billing->getLastname();
			$store = Mage::app()->getStore();
			$shop_page_title = $store->getName();
			$sender_name = Mage::getStoreConfig('trans_email/ident_sales/name');
			$sender_email = Mage::getStoreConfig('trans_email/ident_sales/email');
			$cust_email = $billing->getEmail();

			$MAIL_BODY="<table cellspacing='0' cellpadding='5' border='0' width='80%' align='center'>
                            <tr><td style='background-color:#D63A00;color:white;font-weight:bold;'>
                            Paylater Payment Confirmation</td></tr>
                            <tr><td  style='border:2px solid #D63A00; padding:5px;'>
                            Dear  ".$cust_name.",<p>Please find below the details of the order<p>
                            The Order ID: ".$order_id."<p>
                            The transaction ID/reference for this order is :".$transactionid."<p>
                            Paylater Transcation Reference :".$vendor_id."<p>
                            Please note this ID as it may be used to track your payment.<p>
                            Best Regards<br>".$shop_page_title ."</td></tr></table>";

			$mail = Mage::getModel('core/email');
			$mail->setToName($cust_name);
			$mail->setToEmail($cust_email);
			$mail->setBody($MAIL_BODY);
			$mail->setSubject('PayLater Payment Confirmation');
			$mail->setFromEmail($sender_email);
			$mail->setFromName($sender_name);
			$mail->setType('html');// YOu can use Html or text as Mail format
			try {
				$mail->send();
			} catch(Exception $e)
			{}
			
			
            $session->setErrorMessage($success_msg);			 
			 $this->_redirect('checkout/onepage/success');
		}
		else 
		{
			$order = Mage::getModel('sales/order');
			$order->loadByIncrementId($order_id);
			 
			if (!$order->getId()) {
				return false;
			}
			 $order->getPayment()->setAdditionalInformation('transacton_reference', $vendor_id);

			$order->addStatusToHistory(
				$order->getStatus(),
				Mage::helper('paylater')->__('Paylater Payment Failed. Order ID :'.$order_id.', Err Message :'.$transactioncode.' - '.$transactionstatus.', Transaction ID :'.$transactionid.'')
			);
			
			 $order->save();
				
											 
			
			
				$session = Mage::getSingleton('checkout/session');		
				$session->setQuoteId($session->getPaylaterStandardQuoteId(true));						 
						 
				$err_msg  ='<b>Order ID : </b>'.$order_id;								
				$err_msg .='<br><b>Transaction ID : </b>'.$transactionid;
				$err_msg .='<br><b>Paylater Transaction Reference : </b>'.$vendor_id;
				$err_msg .= '<br><b>Reason : </b><span style="color:#ff0000;">'.$transactionstatus.'</span>';	


				$history = Mage::helper('paylater')->__($err_msg);
				$session->setErrorMessage($err_msg);			 
				$redirectTo = 'paylater/standard/failure';
				$this->_redirect($redirectTo);			
		 }
    }

    /**
     *  Save invoice for order
     *
     *  @param    Mage_Sales_Model_Order $order
     *  @return	  boolean Can save invoice or not
     */
    protected function saveInvoice (Mage_Sales_Model_Order $order)
    {
        if ($order->canInvoice()) {
            $invoice = $order->prepareInvoice();

            $invoice->register()->capture();
            Mage::getModel('core/resource_transaction')
               ->addObject($invoice)
               ->addObject($invoice->getOrder())
               ->save();
            return true;
        }

        return false;
    }


    /**
     *  Expected GET HTTP Method
     *
     *  @return	  void
     */
    protected function preResponse ()
    {
        $this->isValidResponse = true;		
		 
    }

    /**
     *  Failure Action	
     *
     *  @return	  void
     */
    public function failureAction ()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setPaylaterStandardQuoteId($session->getQuoteId());
		
        if (!$session->getErrorMessage()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('paylater/session');
        $this->renderLayout();
    }
	

     public function successAction ()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setPaylaterStandardQuoteId($session->getQuoteId());
		
        if (!$session->getErrorMessage()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('paylater/session');
        $this->renderLayout();

    }
	
     public function queryAction ()
    {
		$this->responseArr=$_REQUEST;
		$orderid=$this->responseArr['order_id'];
		$order = Mage::getModel('sales/order');
                $order->loadByIncrementId($this->responseArr['order_id']);
		$amount = $order->getBaseGrandTotal();
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		$table = "mgn_paylater_api_debug";
		$query = "SELECT * FROM  {$table}  WHERE  order_id='".$orderid."' ORDER BY id";
		$readresult=$writeConnection->query($query);	
		while($row=$readresult->fetch()) 
		{
			$order_id =$row['order_id'];
			$amount =$row['amount'];
			$transn_id =$row['transn_id'];
                        $vendor_id=$row['vendor_id'];
                        $vendor=$row['vendor'];
			$ID_MOD =$row['id'];
			$transactioncode =$row['transactioncode'];
			$transactionstatus  =$row['transactionstatus'];

		}
		$response= $this->getStatus($transn_id, $vendor_id, $vendor);
                $responseArray= json_decode($response,true);
		$sql="UPDATE {$table} SET transactioncode='".$responseArray['code']."',
                    transactionstatus='".$responseArray['status']."',post_time='".time()."' WHERE id='".$ID_MOD."' ";
		$writeConnection->query($sql);

		echo "<div id='sales_order_view_tabs_adminhtml_order_view_tab_query_val_{$id}'>";
		echo "<hr><b>PayLater Status :</b>".$responseArray['transactionstatus'].'<br>';			
		echo "<b>PayLater Transaction Id :</b>".$transn_id.'<br>';									
		echo "<b>PayLater Transaction Reference :</b>".$responseArray['transref'].'<br>';
		echo "</div>";


    } 			
}