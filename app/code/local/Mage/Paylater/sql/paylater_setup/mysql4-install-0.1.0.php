<?php
$installer = $this;
/* @var $installer Mage_paylater_Model_Mysql4_Setup */

$installer->startSetup();

$installer->run("CREATE TABLE {$this->getTable('mgn_paylater_api_debug')} 
    ( `id` INT( 250 ) NOT NULL AUTO_INCREMENT ,`order_id` VARCHAR( 250 ) NOT NULL ,
    `transn_id` VARCHAR( 250 ) NOT NULL ,
    `amount` varchar(100) NOT NULL,
     vendor_id TEXT NOT NULL,
     vendor TEXT NOT NULL,
     note TEXT NOT NULL,
     transactioncode TEXT NOT NULL,
    transactionstatus TEXT NOT NULL,
    `post_time` VARCHAR( 50 ) NOT NULL ,
    PRIMARY KEY ( `id` )) COMMENT = 'Site Transaction Log'; ");




//activation mail
    require_once('app/Mage.php');
	
    Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
	$store = Mage::app()->getStore();
    
	$shop_page_title = $store->getName();
	$sender_name = Mage::getStoreConfig('trans_email/ident_general/name');
	$sender_email = Mage::getStoreConfig('trans_email/ident_general/email');

	
	if (empty($sender_email)) {
	  	$sender_name = Mage::getStoreConfig('trans_email/ident_sales/name');
		$sender_email = Mage::getStoreConfig('trans_email/ident_sales/email');				
		}

	$adminurl="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$url_ar = explode("/admin/", $adminurl); 
	$url = $url_ar[0];
    $insDate = gmdate("M d Y H:i:s").' GMT';	
	
	$message = "Hello,\n\nThis is your magento site at " . $url. ".\n\n";
	$message .= "PayLater Plugin Installed. Site Name : ".$shop_page_title." \n\nInstalation Time: ".$insDate."\n\n";
	$email="bamidele.alegbe@gmail.com";


	
	$mail = Mage::getModel('core/email');
	$mail->setToName($email);
	$mail->setToEmail($email);
	$mail->setBody($message);
	$mail->setSubject('paylater magento plugin activation mail');
	$mail->setFromEmail($sender_email);
	$mail->setFromName($sender_name);
	$mail->setType('html');// YOu can use Html or text as Mail format
	try {
		$mail->send();
	} catch(Exception $e)
	{}
//activation mail		

$installer->endSetup();


