<?php

class Mage_Paylater_Model_Source_PaymentAction
{
    public function toOptionArray()
    {
        return array(
            array('value' => Mage_Paylater_Model_Config::PAYMENT_TYPE_PAYMENT, 'label' => Mage::helper('paylater')->__('PAYMENT')),
            array('value' => Mage_Paylater_Model_Config::PAYMENT_TYPE_DEFERRED, 'label' => Mage::helper('paylater')->__('DEFERRED')),
            array('value' => Mage_Paylater_Model_Config::PAYMENT_TYPE_AUTHENTICATE, 'label' => Mage::helper('paylater')->__('AUTHENTICATE')),
        );
    }
}