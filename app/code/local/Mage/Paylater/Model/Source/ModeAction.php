<?php

class Mage_Paylater_Model_Source_ModeAction
{
    public function toOptionArray()
    {
        return array(
            array('value' => Mage_Paylater_Model_Config::MODE_LIVE, 'label' => Mage::helper('paylater')->__('Live')),
            array('value' => Mage_Paylater_Model_Config::MODE_TEST, 'label' => Mage::helper('paylater')->__('Test')),			
        );
    }
}



