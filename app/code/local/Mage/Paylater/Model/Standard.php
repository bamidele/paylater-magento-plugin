<?php

class Mage_Paylater_Model_Standard extends Mage_Payment_Model_Method_Abstract
{
    protected $_code  = 'paylater_standard';
    protected $_formBlockType = 'paylater/standard_form';
    protected $_infoBlockType = 'paylater/standard_info';
	
    protected $_isGateway               = false;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = true;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = false;

    protected $_order = null;


    /**
     * Get Config model
     *
     * @return object Mage_Paylater_Model_Config
     */
    public function getConfig()
    {
        return Mage::getSingleton('paylater/config');
    }

/**
	 * Return the payment info model instance for the order
	 *
	 * @return Mage_Payment_Model_Info
	 */
	public function getInfoInstance()
	{
		$payment = $this->getData('info_instance');
		if (! $payment)
		{
			$payment = $this->getOrder()->getPayment();
			$this->setInfoInstance($payment);
		}
		return $payment;
	}

	
/**
	 * Return the specified additional information from the payment info instance
	 *
	 * @param string $key
	 * @param Varien_Object $payment
	 * @return string
	 */
	public function getPaymentInfoData($key, $payment = null)
	{
        if (is_null($payment))
		{
			$rand=rand(1,9999);
			$timesammp=DATE("dmyHis").$rand;		
		    $transactionId = $timesammp;
			$payment = $this->getInfoInstance();
    		$payment->setAdditionalInformation('transaction_id', $transactionId);			
			$payment->save;
		}
		return $payment->getAdditionalInformation($key);
	}	
	
	/**
	 * Return the transaction id for the current transaction
	 *
	 * @return string
	 */
	public function getTransactionId()
	{
		return $this->getPaymentInfoData('transaction_id');
	}	
	

    /**
     * Assign data to info model instance
     *
     * @param   mixed $data
     * @return  Mage_Payment_Model_Info
     */
    public function assignData($data)
    {
        $result = parent::assignData($data);     
   		 $details = array();   
    	 $details['transn_id'] = $this->getTransactionId();
		 $details['order_id'] = $this->getVendorTxCode();
		 $details['validate_url'] = $this->getPaylaterUrlVerify();
         if (!empty($details)) {
            $this->getInfoInstance()->setAdditionalData(serialize($details));
         }		 

       return $result;
    }	
    /**
     * Return debug flag
     *
     *  @return  boolean
     */
    public function getDebug ()
    {
        return $this->getConfig()->getDebug();
    }

    public function getVendorId()
    {
        return $this->getConfig()->getVendorId();
    }
    public function getVendor()
    {
        return $this->getConfig()->getVendor();
    }
    /**
     *  Returns Target URL
     *
     *  @return	  string Target URL
     */
   
    
    public function getPaylaterUrl ()
    {
        switch ($this->getConfig()->getMode()) {
            case Mage_Paylater_Model_Config::MODE_LIVE:
                $url = 'https://app.paylater.ng/gateway';
                break;
            case Mage_Paylater_Model_Config::MODE_TEST:
                $url = 'https://sandbox.paylater.ng/gateway';
                break;
            default: // simulator mode
                $url = 'https://sandbox.paylater.ng/gateway';
                break;
        }
        return $url;
        
    }

    public function getPaylaterUrlVerify ()
    {
        return $this->getConfig()->getVerifyUrl();
    }
	
	
   

    /**
     *  Return URL for Paylater success response
     *
     *  @return	  string URL
     */
    protected function getSuccessURL ()
    {
        return Mage::getUrl('paylater/standard/successresponse');
    }

    /**
     *  Return URL for Paylater failure response
     *
     *  @return	  string URL
     */
    protected function getFailureURL ()
    {
        return Mage::getUrl('paylater/standard/failureresponse');
    }

    /**
     * Transaction unique ID sent to Paylater and sent back by Paylater for order restore
     * Using created order ID
     *
     *  @return	  string Transaction unique number
     */
    protected function getVendorTxCode ()
    {
		$ORDER=$this->getOrder();
		 if(!empty($ORDER))
			 $realorderId=$ORDER->getRealOrderId();
		 else
			 $realorderId=rand(1,9999999);
		return $realorderId;		
    }

    /**
     *  Returns cart formatted
     *  String format:
     *  Number of lines:Name1:Quantity1:CostNoTax1:Tax1:CostTax1:Total1:Name2:Quantity2:CostNoTax2...
     *
     *  @return	  string Formatted cart items
     */
    protected function getFormattedCart ()
    {
        $items = $this->getOrder()->getAllItems();
        $resultParts = array();
        $totalLines = 0;
        if ($items) {
            foreach($items as $item) {
                if ($item->getParentItem()) {
                    continue;
                }
                $quantity = $item->getQtyOrdered();

                $cost = sprintf('%.2f', $item->getBasePrice() - $item->getBaseDiscountAmount());
                $tax = sprintf('%.2f', $item->getBaseTaxAmount());
                $costPlusTax = sprintf('%.2f', $cost + $tax/$quantity);

                $totalCostPlusTax = sprintf('%.2f', $quantity * $cost + $tax);

                $resultParts[] = str_replace(':', ' ', $item->getName());
                $resultParts[] = $quantity;
                $resultParts[] = $cost;
                $resultParts[] = $tax;
                $resultParts[] = $costPlusTax;
                $resultParts[] = $totalCostPlusTax;
                $totalLines++; //counting actual formatted items
            }
       }

       // add delivery
       $shipping = $this->getOrder()->getBaseShippingAmount();
       if ((int)$shipping > 0) {
           $totalLines++;
           $resultParts = array_merge($resultParts, array('Shipping','','','','',sprintf('%.2f', $shipping)));
       }

       $result = $totalLines . ':' . implode(':', $resultParts);
       return $result;
    }


    /**
     *  Form block description
     *
     *  @return	 object
     */
    public function createFormBlock($name)
    {
        $block = $this->getLayout()->createBlock('paylater/form_standard', $name);
        $block->setMethod($this->_code);
        $block->setPayment($this->getPayment());
        return $block;
    }

 /**
     *  Return Order Place Redirect URL
     *
     *  @return	  string Order Redirect URL
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('paylater/standard/redirect');
    }    

    /**
     *  Return Standard Checkout Form Fields for request to Paylater
     *
     *  @return	  array Array of hidden form fields
     */
    public function getStandardCheckoutFormFields ()
    {
            $vendorid = $this->getVendorId();

            // Your Store name as registered with us
            $vendor = $this->getVendor();

            

            // transaction amount
            //$amount = 1000.44;

            // Transaction reference number. must be unique to each transaction. This will be used to requery the transaction later
           

            // Details of the transaction. it should include the product purchased and the customer name and email
            

            // absolute url of the page to which the user should be directed after payment
            // an example of the code needed in this type of page can be found in Requery_transaction.php
    
        $order = $this->getOrder();		
        $amount = $order->getBaseGrandTotal();
        $description = $this->getVendor(). ' - ' . ' payment';
		$transactionId = $this->getPaymentInfoData('transaction_id', $order->getPayment());
			
		 $order->addStatusToHistory(
            $order->getStatus(),
            Mage::helper('paylater')->__('Paylater Order ID :'.$transactionId)
        );
		
        $order->save();



				
        $billing = $order->getBillingAddress();		
	//$cust_id = $order->getCustomerId();
        $cust_name = $billing->getFirstname().' '.$billing->getLastname();
		
		//send mail
		$store = Mage::app()->getStore();
		$shop_page_title = $store->getName();
		$sender_name = Mage::getStoreConfig('trans_email/ident_sales/name');
		$sender_email = Mage::getStoreConfig('trans_email/ident_sales/email');
		$cust_email = $billing->getEmail();

		$MAIL_BODY="<table cellspacing='0' cellpadding='5' border='0' width='80%' align='center'>
                    <tr><td style='background-color:#D63A00;color:white;font-weight:bold;'>
                    Paylater Transaction</td></tr>
                    <tr><td  style='border:2px solid #D63A00; padding:5px;'>
                    Dear  ".$cust_name.",<p>Please find below the details of the order<p>
                    The Order ID: ".$this->getVendorTxCode()."<p>
                    The transaction ID/reference for this order is :".$transactionId."<p>
                    Please note this ID as it may be used to track your payment.<p>
                    Best Regards<br>".$shop_page_title ."</td></tr></table>";

		$mail = Mage::getModel('core/email');
		$mail->setToName($cust_name);
		$mail->setToEmail($cust_email);
		$mail->setBody($MAIL_BODY);
		$mail->setSubject('Paylater Payment Notification');
		$mail->setFromEmail($sender_email);
		$mail->setFromName($sender_name);
		$mail->setType('html');// YOu can use Html or text as Mail format
		try {
			$mail->send();
		} catch(Exception $e)
		{}

		


		$orderid=$this->getVendorTxCode();
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		$table = "mgn_paylater_api_debug";	 
		$query = "INSERT INTO {$table} 
                    (order_id,transn_id,amount,vendor_id,vendor,note,transactioncode,transactionstatus,post_time)
                  VALUES ('$orderid','$transactionId','$amount','$vendorid','$vendor','$description','nill','nill','".time()."' )";
            
		$writeConnection->query($query);	
		 
		//}
		$notifyURL=$this->getSuccessURL();
                switch ($this->getConfig()->getMode()) {
                    case Mage_Paylater_Model_Config::MODE_LIVE:
                    $email = $cust_email;
                    break;
                    case Mage_Paylater_Model_Config::MODE_TEST:
                    $email = 'test@test.com';
                    break;
                    default: // simulator mode
                    $email = 'test@test.com';
                    break;
                }
		/********INSERT DATA ********/
		$fields = array(
				'vendorid' 		=> $vendorid,
                                'vendor' 		=> $vendor,
                                'email'			=> $email,
				'amount' 		=> sprintf('%.2f', $amount),
				'transref' 		=> $transactionId,
				'note'	=> $description,
				'redirecturl'	=> $notifyURL
				
				
			);
			
	
		  
          return $fields;
    }
}